import React from 'react';
import { shallow } from 'enzyme';
import { DefaultPage } from '../../../src/features/capture/DefaultPage';

describe('capture/DefaultPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      capture: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <DefaultPage {...props} />
    );

    expect(
      renderedComponent.find('.capture-default-page').length
    ).toBe(1);
  });
});
