import React from 'react';
import { shallow } from 'enzyme';
import { MyForm } from '../../../src/features/capture/MyForm';

describe('capture/MyForm', () => {
  it('renders node with correct class name', () => {
    const props = {
      capture: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <MyForm {...props} />
    );

    expect(
      renderedComponent.find('.capture-my-form').length
    ).toBe(1);
  });
});
