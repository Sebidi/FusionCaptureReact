import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  CAPTURE_MEDIA_ITEM_BEGIN,
  CAPTURE_MEDIA_ITEM_SUCCESS,
  CAPTURE_MEDIA_ITEM_FAILURE,
  CAPTURE_MEDIA_ITEM_DISMISS_ERROR,
} from '../../../../src/features/capture/redux/constants';

import {
  mediaItem,
  dismissMediaItemError,
  reducer,
} from '../../../../src/features/capture/redux/mediaItem';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('capture/redux/mediaItem', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when mediaItem succeeds', () => {
    const store = mockStore({});

    return store.dispatch(mediaItem())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CAPTURE_MEDIA_ITEM_BEGIN);
        expect(actions[1]).toHaveProperty('type', CAPTURE_MEDIA_ITEM_SUCCESS);
      });
  });

  it('dispatches failure action when mediaItem fails', () => {
    const store = mockStore({});

    return store.dispatch(mediaItem({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CAPTURE_MEDIA_ITEM_BEGIN);
        expect(actions[1]).toHaveProperty('type', CAPTURE_MEDIA_ITEM_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissMediaItemError', () => {
    const expectedAction = {
      type: CAPTURE_MEDIA_ITEM_DISMISS_ERROR,
    };
    expect(dismissMediaItemError()).toEqual(expectedAction);
  });

  it('handles action type CAPTURE_MEDIA_ITEM_BEGIN correctly', () => {
    const prevState = { mediaItemPending: false };
    const state = reducer(
      prevState,
      { type: CAPTURE_MEDIA_ITEM_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.mediaItemPending).toBe(true);
  });

  it('handles action type CAPTURE_MEDIA_ITEM_SUCCESS correctly', () => {
    const prevState = { mediaItemPending: true };
    const state = reducer(
      prevState,
      { type: CAPTURE_MEDIA_ITEM_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.mediaItemPending).toBe(false);
  });

  it('handles action type CAPTURE_MEDIA_ITEM_FAILURE correctly', () => {
    const prevState = { mediaItemPending: true };
    const state = reducer(
      prevState,
      { type: CAPTURE_MEDIA_ITEM_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.mediaItemPending).toBe(false);
    expect(state.mediaItemError).toEqual(expect.anything());
  });

  it('handles action type CAPTURE_MEDIA_ITEM_DISMISS_ERROR correctly', () => {
    const prevState = { mediaItemError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: CAPTURE_MEDIA_ITEM_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.mediaItemError).toBe(null);
  });
});

