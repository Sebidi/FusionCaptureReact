import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  CAPTURE_WORK_ITME_ATTRI_BEGIN,
  CAPTURE_WORK_ITME_ATTRI_SUCCESS,
  CAPTURE_WORK_ITME_ATTRI_FAILURE,
  CAPTURE_WORK_ITME_ATTRI_DISMISS_ERROR,
} from '../../../../src/features/capture/redux/constants';

import {
  workItmeAttri,
  dismissWorkItmeAttriError,
  reducer,
} from '../../../../src/features/capture/redux/workItmeAttri';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('capture/redux/workItmeAttri', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when workItmeAttri succeeds', () => {
    const store = mockStore({});

    return store.dispatch(workItmeAttri())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CAPTURE_WORK_ITME_ATTRI_BEGIN);
        expect(actions[1]).toHaveProperty('type', CAPTURE_WORK_ITME_ATTRI_SUCCESS);
      });
  });

  it('dispatches failure action when workItmeAttri fails', () => {
    const store = mockStore({});

    return store.dispatch(workItmeAttri({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', CAPTURE_WORK_ITME_ATTRI_BEGIN);
        expect(actions[1]).toHaveProperty('type', CAPTURE_WORK_ITME_ATTRI_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissWorkItmeAttriError', () => {
    const expectedAction = {
      type: CAPTURE_WORK_ITME_ATTRI_DISMISS_ERROR,
    };
    expect(dismissWorkItmeAttriError()).toEqual(expectedAction);
  });

  it('handles action type CAPTURE_WORK_ITME_ATTRI_BEGIN correctly', () => {
    const prevState = { workItmeAttriPending: false };
    const state = reducer(
      prevState,
      { type: CAPTURE_WORK_ITME_ATTRI_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workItmeAttriPending).toBe(true);
  });

  it('handles action type CAPTURE_WORK_ITME_ATTRI_SUCCESS correctly', () => {
    const prevState = { workItmeAttriPending: true };
    const state = reducer(
      prevState,
      { type: CAPTURE_WORK_ITME_ATTRI_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workItmeAttriPending).toBe(false);
  });

  it('handles action type CAPTURE_WORK_ITME_ATTRI_FAILURE correctly', () => {
    const prevState = { workItmeAttriPending: true };
    const state = reducer(
      prevState,
      { type: CAPTURE_WORK_ITME_ATTRI_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workItmeAttriPending).toBe(false);
    expect(state.workItmeAttriError).toEqual(expect.anything());
  });

  it('handles action type CAPTURE_WORK_ITME_ATTRI_DISMISS_ERROR correctly', () => {
    const prevState = { workItmeAttriError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: CAPTURE_WORK_ITME_ATTRI_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workItmeAttriError).toBe(null);
  });
});

