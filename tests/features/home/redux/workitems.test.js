import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  HOME_WORKITEMS_BEGIN,
  HOME_WORKITEMS_SUCCESS,
  HOME_WORKITEMS_FAILURE,
  HOME_WORKITEMS_DISMISS_ERROR,
} from '../../../../src/features/home/redux/constants';

import {
  workitems,
  dismissWorkitemsError,
  reducer,
} from '../../../../src/features/home/redux/workitems';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('home/redux/workitems', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when workitems succeeds', () => {
    const store = mockStore({});

    return store.dispatch(workitems())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_WORKITEMS_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_WORKITEMS_SUCCESS);
      });
  });

  it('dispatches failure action when workitems fails', () => {
    const store = mockStore({});

    return store.dispatch(workitems({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', HOME_WORKITEMS_BEGIN);
        expect(actions[1]).toHaveProperty('type', HOME_WORKITEMS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissWorkitemsError', () => {
    const expectedAction = {
      type: HOME_WORKITEMS_DISMISS_ERROR,
    };
    expect(dismissWorkitemsError()).toEqual(expectedAction);
  });

  it('handles action type HOME_WORKITEMS_BEGIN correctly', () => {
    const prevState = { workitemsPending: false };
    const state = reducer(
      prevState,
      { type: HOME_WORKITEMS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workitemsPending).toBe(true);
  });

  it('handles action type HOME_WORKITEMS_SUCCESS correctly', () => {
    const prevState = { workitemsPending: true };
    const state = reducer(
      prevState,
      { type: HOME_WORKITEMS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workitemsPending).toBe(false);
  });

  it('handles action type HOME_WORKITEMS_FAILURE correctly', () => {
    const prevState = { workitemsPending: true };
    const state = reducer(
      prevState,
      { type: HOME_WORKITEMS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workitemsPending).toBe(false);
    expect(state.workitemsError).toEqual(expect.anything());
  });

  it('handles action type HOME_WORKITEMS_DISMISS_ERROR correctly', () => {
    const prevState = { workitemsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: HOME_WORKITEMS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.workitemsError).toBe(null);
  });
});

