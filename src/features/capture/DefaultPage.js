import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import {Button} from 'reactstrap';
import ToggleDisplay from 'react-toggle-display';
import MyForm from './MyForm';
import { Form, FormGroup, Label, Input } from 'reactstrap';


export class DefaultPage extends Component {
  static propTypes = {
    capture: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
   constructor() {
    super();
    this.state = { 
      showSponsor: false , 
      showDupli :false, 
      radio: false, 
      tv: false,
      arrayTest:[]
      }
  }
 
  Advertising() {
    this.setState({
      showSponsor: !this.state.showSponsor
    });
  }
  Editorial() {
    this.setState({
      showDupli: !this.state.showDupli
    });
  }
  componentDidMount(){
    this.props.actions.mediaItem();
    this.props.actions.workItmeAttri();
    this.checkMedia();
    this.getAttributes();

    if(this.props.capture.id==="ADVERTISING")
    {
      this.Advertising();
    }else if(this.props.capture.id==="EDITORIAL"){
      this.Editorial();
    }
    this.setState({arrayTest:this.props.capture.workItemAttr})
   
  }

  checkMedia(){
    if(this.props.capture.mediaName === "Radio")
    {
      this.setState({radio:true});
    }      
    if(this.props.capture.mediaName === "Tv"){
      this.setState({radio:true});
    }
  }
  getAttributes =() =>{
    var tr = JSON.stringify([this.props.capture.workItemAttr]);
    console.log(tr);
  }

  render() 
  {
    return (
      <div className="capture-default-page">
        <div className="home-default-page">
          <header className="app-header">
            <h1 className="app-title">Fusion Capture</h1>
          </header>
          <div className="app-intro">
             
             <ToggleDisplay if={this.state.showDupli}>
              <h1>Duplicate</h1>
              <MyForm/>
             </ToggleDisplay>

             <ToggleDisplay if={this.state.showSponsor}>
             </ToggleDisplay>
             <ToggleDisplay if={this.state.tv}>
                <h1>TV</h1>
             </ToggleDisplay>
              <ToggleDisplay if={this.state.radio}>
                <MyForm/>
                {this.getAttributes()}
              </ToggleDisplay>

          </div>
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    capture: state.capture,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
