import axios from 'axios';
import {
  CAPTURE_MEDIA_ITEM_BEGIN,
  CAPTURE_MEDIA_ITEM_SUCCESS,
  CAPTURE_MEDIA_ITEM_FAILURE,
  CAPTURE_MEDIA_ITEM_DISMISS_ERROR,
} from './constants';

// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function mediaItem(args = {}) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: CAPTURE_MEDIA_ITEM_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      const token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI0MjI4YWMxYi0wZGJlLTQzYzUtODA0Zi1hYTAzOTUyZmZhZmMiLCJpYXQiOjE1MjI3NjUzMjUsInNjb3BlIjoyLCJleHAiOjE1MjMzNzAxMjUsImlkIjoiR01WeFNVZDNlYWs9In0.u-z8Zae5SBKNv77wNI-IOhMDiqs1r_fHJ5sgpCpDqdk";
      const mediaId=sessionStorage.getItem("mediaId");
      console.log(mediaId);
      let configMediaItem = {
        url:"http://devtest:4200/graphql",
        method: 'post',
        headers: {
            "Authorization": token,
        },
        data: {
            query: `query($id:UUID!){
                     mediaItem(id:$id){
                     active
                     webPath
                     flightDate
                     source{name}
                     source{ viewership }
                     next{webPath source{name}}
                  sourcePosition
                  source{mediaType{name}}
                  }
          }`,
            variables: {
                id: mediaId
            }
        }
      };

      axios(configMediaItem).then(
        (res) => {
          dispatch({
            type: CAPTURE_MEDIA_ITEM_SUCCESS,
            data: res.data.data.mediaItem,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: CAPTURE_MEDIA_ITEM_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissMediaItemError() {
  return {
    type: CAPTURE_MEDIA_ITEM_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case CAPTURE_MEDIA_ITEM_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        mediaItemPending: true,
        mediaItemError: null,
      };

    case CAPTURE_MEDIA_ITEM_SUCCESS:
      // The request is success
       return {
        ...state,
        mediaItemPending: false,
        mediaItemError: null,
        media: action.data,
        mediaName: action.data.source.mediaType.name
      };

    case CAPTURE_MEDIA_ITEM_FAILURE:
      // The request is failed
      return {
        ...state,
        mediaItemPending: false,
        mediaItemError: action.data.error,
      };

    case CAPTURE_MEDIA_ITEM_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        mediaItemError: null,
      };

    default:
      return state;
  }
}
