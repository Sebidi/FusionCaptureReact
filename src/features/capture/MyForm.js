import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Form, FormGroup, Label, Col , Container, Row, Input,Button} from 'reactstrap';
import ReactPlayer from 'react-player'
import ToggleDisplay from 'react-toggle-display';
import {Icon} from 'react-fa';


export class MyForm extends Component {
  static propTypes = {
    capture: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor(){
    super();
    this.state={
      crop:"",
      textBoxStartCrop:"",
      textBoxEndCrop:"",
      startHide:true,
      endHide:false,
      processHide:false,
    }
    this.progress = this.progress.bind(this);
    this.startCrop = this.startCrop.bind(this);
    this.endCrop = this.endCrop.bind(this);
    this.processCrop = this.processCrop.bind(this);
    this.endErase = this.endErase.bind(this);
    this.startErase = this.startErase.bind(this);   
  }
  progress(e){
    this.setState({crop:e.played});
  }
  startCrop(){
    this.setState({textBoxStartCrop: this.state.crop});
    this.setState({startHide: !this.state.startHide});
    this.setState({endHide: !this.state.endHide});
  }
  endCrop(){
    this.setState({textBoxEndCrop: this.state.crop});
    this.setState({endHide: !this.state.endHide});
    this.setState({processHide: !this.state.processHide});
  }
  processCrop(){

  }
  endErase(){
    this.setState({textBoxEndCrop:""});
    this.setState({processHide: !this.state.processHide});
    this.setState({endHide: !this.state.endHide});
  }
  startErase(){
    this.setState({textBoxStartCrop: ""});
    this.setState({startHide: !this.state.startHide});
    if(this.state.textBoxEndCrop!=="")
    {
      this.setState({textBoxEndCrop: ""});
    }
    if(this.state.processHide===true)
    {
      this.setState({processHide: false});
    }
    if(this.state.endHide===true)
    {
      this.setState({endHide: false});
    }

  }
  render() {
    return (
      <div className="capture-my-form">
       <Container>
        <Row>
          <Col xs="3">
            <h1>Data Capture</h1><hr/>
            <Form>
              <FormGroup>
                <Label for="starttime">Start Time</Label>
                <Input type="starttime" name="starttime" id="starttime" placeholder="with a placeholder" />
              </FormGroup>
             </Form>
             <Button color="primary">Save</Button>{' '}
          </Col>
          <Col xs="9"><h1>Media</h1><hr/>
            <ReactPlayer
              url='https://vimeo.com/243556536'
              className='react-player'
              controls
              progressFrequency
              width='100%'
              height='100%'
              onProgress={this.progress}
            />
            <Form><hr/>
              <FormGroup row>
                <Label for="startCrop" sm={2}>Start Crop</Label>
                  <Col sm={4}>
                    <Input type="startCrop" name="startCrop" id="startCrop" placeholder="Start Crop....." value={this.state.textBoxStartCrop} />
                  </Col>
                  <Col sm={2}>
                    <Button color="danger" onClick={this.startErase}><i className="fa fa-trash"></i></Button>
                  </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="endCrop" sm={2}>End Crop</Label>
                <Col sm={4}>
                  <Input type="endCrop" name="endCrop" id="endCrop" placeholder="End Crop....." value={this.state.textBoxEndCrop}/>
                </Col>
                <Col sm={2}>
                  <Button color="danger" onClick={this.endErase}><i className="fa fa-trash"></i></Button>
                </Col>
              </FormGroup><hr/>
               <FormGroup row>
                <Col sm={10}>
                  <ToggleDisplay if={this.state.startHide}>
                    <Button color="success" onClick={this.startCrop}>Start Crop </Button>{' '}
                  </ToggleDisplay>
                  <ToggleDisplay if={this.state.endHide}>
                    <Button color="danger" onClick={this.endCrop}>End Crop </Button>{' '}
                  </ToggleDisplay>
                  <ToggleDisplay if={this.state.processHide}>
                    <Button color="success" onClick={this.processCrop}>Process crop </Button>{' '}
                  </ToggleDisplay>
                </Col>
               </FormGroup>
            </Form>
          </Col>
        </Row>
       </Container>
      </div> 
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    capture: state.capture,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyForm);
