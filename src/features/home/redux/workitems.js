import axios from 'axios';
import {
  HOME_WORKITEMS_BEGIN,
  HOME_WORKITEMS_SUCCESS,
  HOME_WORKITEMS_FAILURE,
  HOME_WORKITEMS_DISMISS_ERROR,
} from './constants';

// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function workitems(args = {}) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: HOME_WORKITEMS_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      const token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI0MjI4YWMxYi0wZGJlLTQzYzUtODA0Zi1hYTAzOTUyZmZhZmMiLCJpYXQiOjE1MjI3NjUzMjUsInNjb3BlIjoyLCJleHAiOjE1MjMzNzAxMjUsImlkIjoiR01WeFNVZDNlYWs9In0.u-z8Zae5SBKNv77wNI-IOhMDiqs1r_fHJ5sgpCpDqdk";
      const query={
        url:"http://devtest:4200/graphql",
        method:"Post",
         headers: {
            "Authorization": token,
        },
        data:{
          query: `query($id:UUID!,$uncompletedOnly:Boolean){
                    user(id:$id){
                    id
                    firstName
                    lastName
                    assignments(completed:$uncompletedOnly){ 
                        workItem
                        {
                                id
                                mediaItemId
                                createdAt
                                active
                                nodes{name}
                                company{name}
                                clip{ 
                                        name
                                        webPath
                                    }
                                contentType
                                completed
                                mediaItem{
                                    source{
                                            name
                                            mediaType{name}
                                        }
                                    webPath
                                    flightDate 
                        }
                    }
                }
            }
        }`, variables:{
            id: "4228ac1b-0dbe-43c5-804f-aa03952ffafc",
	          uncompletedOnly:false
           }
        }
      }

      axios(query).then(
        (res) => {
          dispatch({
            type: HOME_WORKITEMS_SUCCESS,
            data: res.data.data.user,
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: HOME_WORKITEMS_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissWorkitemsError() {
  return {
    type: HOME_WORKITEMS_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case HOME_WORKITEMS_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        workitemsPending: true,
        workitemsError: null,
      };

    case HOME_WORKITEMS_SUCCESS:
      // The request is success
      const obj = [];
      action.data.assignments.forEach(function(item){
          obj.push(item);
      });

      return {
        ...state,
        workitemsPending: false,
        workitemsError: null,
        listWorkItems:obj,
        fisrtName: action.data.firstName,
      };

    case HOME_WORKITEMS_FAILURE:
      // The request is failed
      return {
        ...state,
        workitemsPending: false,
        workitemsError: action.data.error,
      };

    case HOME_WORKITEMS_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        workitemsError: null,
      };

    default:
      return state;
  }
}
