import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import reactLogo from '../../images/react-logo.svg';
import rekitLogo from '../../images/rekit-logo.svg';
import * as actions from './redux/actions';
import SortableTbl from 'react-sort-search-table';
import {Icon} from 'react-fa';
import {Button} from 'reactstrap';

export class DefaultPage extends Component {
  static propTypes = {
    home: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };
  constructor(props){
    super();
    this.state={
      clip:[],
    }
  }
  componentDidMount(){
    this.props.actions.workitems();
    this.setState({ clip: this.props.home.listWorkItems.map(obj=>obj.workItem)});

  }
  componentSourceName=(props)=>{
    return(
      <td>
           {props.rowData.clip.webPath}
      </td>
    )
  }
  componentWebPath=(props)=>{
    return(
      <td>
           {props.rowData.mediaItem.source.name}
      </td>
    )
  }
  componentFlightDate=(props)=>{
    return(
      <td>
           {props.rowData.mediaItem.flightDate}
      </td>
    )
  }
  componentMediaType=(props)=>{
    return(
      <td>
           {props.rowData.mediaItem.source.mediaType.name}
      </td>
    )
  }
  session(id,content,mediaId){
    sessionStorage.setItem('key',id);
    sessionStorage.setItem('content',content);
    sessionStorage.setItem('mediaId',mediaId);
  }
  componentAction=(props)=>{
    return(
      <td>
         <Link to="/capture"><Button color="success" onClick={this.session(props.rowData.id,props.rowData.contentType,props.rowData.mediaItemId)}><a className="fa fa-hourglass-end"> Process</a></Button></Link>
      </td>
    )
  }
  render(props) {
  console.log(this.props.home.fisrtName);
    let col = [
        "contentType",
        "clip",
        "sourcename",
        "mediaType",
        "flightDate",
        "process",
    ];
    let tHead = [
        "Content Type",
        "Clip Path",
        "Source Name",
        "Media Type",	
        "Flight Date",
        "Action",		
    ];
    return (
      <div className="home-default-page">
        <header className="app-header">
          <h1 className="app-title">Fusion Capture</h1>
        </header>
        <h2 className="workersName">Work Items of - {this.props.home.fisrtName}</h2>
        <div className="app-intro"><hr/>
          <SortableTbl tblData={this.props.home.listWorkItems.map(obj=>obj.workItem)}
            tHead={tHead}
            customTd={[
                        {custd: this.componentWebPath, keyItem: "clip"},
                        {custd: this.componentSourceName, keyItem: "sourcename"},
                        {custd: this.componentMediaType, keyItem: "mediaType"},
                        {custd: this.componentFlightDate, keyItem: "flightDate"},
                        {custd: this.componentAction, keyItem: "process"},
                    ]}
            dKey={col}
        />
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    home: state.home,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
